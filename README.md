<h1>Barebones STM32F103 SDR</h1>

I was curious to see the ADC performance of the STM32 family, learn more about digital signal processing, direct memory access and libopencm3. Among other things. The current target is audio quality and selectivity, so no fft spectrum display yet. Currently SSB reception is working. All insight, feedback and help is warmly welcomed.

In this project the mcu samples quadrature audio frequency signals resulting from a tayloe detector, no i2s used here. 

The ADC sampling happens in dual mode, so the two channels are captured simultaneously, this is important as these audio samples need to be kept at a 90 degree phase offset.
DMA is used to store the raw ADC samples and filtered PWM output. Both DMA buffers are the same length and fill up with the same timer, this avoids pitch shifting in the audio.
Once the ADC DMA buffer is half full an interrupt is generated. Next the 16 bit individual channel samples are acquired from the dual adc 32 bit word. We process the lower half of the dma buffer as we know that data is not being updated, on the second transfer complete interrupt its the upper halfs turn. Two CMSIS DSP FIR filter instances process the two channels.

The fixed point FIR filters are fed coefficients generated with the Iowa Hills Hilbert Filter Designer software. These filters impose a +45 and -45 degree phase shift on the samples after which they are multiplied together greatly attenuating the unwanted sideband, this is our target for SSB reception. The processed product is stored in the PWM DMA buffer that is configured to output from a peripherial pin.
Headphones have been sufficient for testing, but the audio path could have a separate amplifier to drive a speaker. Audio is mono.

In my opinion the performance of this simple STM32 setup is amazing. The dual ADC works like a charm and the receive audio quality is on par with a good commercial superhet. With a decent sampling rate and coefficient count the 72MHz factory maximum clock works well, but for the fireworks I've opted to overclock the STM32F103 to 128MHz. The coefficient count and sample rate are always a compromise. For example I can sample at 68kHz with a say 70 or 80 coefficients, but the SSB filter won't be as narrow. A sample rate of 36kHz with 80+ coefficients works well for me.

There are many things I plan to implement and all help is very welcome. I would like to have AGC that is triggered by the ADC watchdog.
I tried decimating the signals without luck. I've been testing the code with sample IQ files from my computer this is a good controlled environment. I've also patched in to the audio channels of the uSDX transceiver to confirm that everything applies on air, which it does. I'm currently designing a simple CNC routed motherboard that mimics the uSDX receiver section replacing the atmega328 with the STM32F103 Blue Pill. I also need to add a test circuit diagram as I have with my [STM32F411 sdr receiver attempt](https://gitlab.com/m0rph/stm32f411_sdr).
