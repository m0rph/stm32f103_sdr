//
// создан автоматически на основании файла: FontFromPentacom.txt
// файл может быть автометически сгенерирован снова. Будьте внимательны и, если внесли в него изменения, сохраните с другим именем

#ifndef TMONITOR_KS0108_FONT
#define TMONITOR_KS0108_FONT

typedef struct
{
  uint8_t size;
  uint8_t l[];
} Ks0108Char_t;

// original was {"33":[0,2,2,2,2,2,0,2,0,0,0,0,0,0,0,0
const Ks0108Char_t char_33 = {2, {0x00, 0xbe}}; // !

// original was "34":[0,10,10,0,0,0,0,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_34 = {4, {0x00, 0x06, 0x00, 0x06}}; // "

// original was "35":[0,20,20,62,20,62,20,20,0,0,0,0,0,0,0,0
const Ks0108Char_t char_35 = {6, {0x00, 0x28, 0xfe, 0x28, 0xfe, 0x28}}; // #

// original was "36":[0,8,60,10,30,40,40,30,8,0,0,0,0,0,0,0
const Ks0108Char_t char_36 = {6, {0x00, 0x98, 0x94, 0xfe, 0x94, 0x64}}; // $

// original was "37":[0,10,8,4,4,2,10,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_37 = {4, {0x00, 0x62, 0x18, 0x46}}; // %

// original was "38":[0,12,18,12,82,34,92,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_38 = {7, {0x00, 0x34, 0x4a, 0x4a, 0x54, 0x20, 0x50}}; // &

// original was "39":[0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_39 = {2, {0x00, 0x06}}; // '

// original was "40":[0,4,2,2,2,2,4,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_40 = {3, {0x00, 0x3c, 0x42}}; // (

// original was "41":[0,2,4,4,4,4,2,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_41 = {3, {0x00, 0x42, 0x3c}}; // )

// original was "42":[0,0,4,14,4,10,0,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_42 = {4, {0x00, 0x28, 0x1c, 0x28}}; // *

// original was "43":[0,8,8,62,8,8,0,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_43 = {6, {0x00, 0x08, 0x08, 0x3e, 0x08, 0x08}}; // +

// original was "44":[0,0,0,0,0,6,4,2,0,0,0,0,0,0,0,0
const Ks0108Char_t char_44 = {3, {0x00, 0xa0, 0x60}}; // ,

// original was "45":[0,0,0,62,0,0,0,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_45 = {6, {0x00, 0x08, 0x08, 0x08, 0x08, 0x08}}; // -

// original was "46":[0,0,0,0,0,6,6,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_46 = {3, {0x00, 0x60, 0x60}}; // .

// original was "47":[0,8,8,4,4,2,2,2,0,0,0,0,0,0,0,0
const Ks0108Char_t char_47 = {4, {0x00, 0xe0, 0x18, 0x06}}; // /

// original was "48":[0,28,34,50,42,38,34,28,0,0,0,0,0,0,0,0
const Ks0108Char_t char_48 = {6, {0x00, 0x7c, 0xa2, 0x92, 0x8a, 0x7c}}; // 0

// original was "49":[0,6,4,4,4,4,4,14,0,0,0,0,0,0,0,0
const Ks0108Char_t char_49 = {4, {0x00, 0x82, 0xfe, 0x80}}; // 1

// original was "50":[0,28,34,32,32,28,2,62,0,0,0,0,0,0,0,0
const Ks0108Char_t char_50 = {6, {0x00, 0xc4, 0xa2, 0xa2, 0xa2, 0x9c}}; // 2

// original was "51":[0,28,34,24,32,32,34,28,0,0,0,0,0,0,0,0
const Ks0108Char_t char_51 = {6, {0x00, 0x44, 0x82, 0x8a, 0x8a, 0x74}}; // 3

// original was "52":[0,16,24,20,18,18,62,16,0,0,0,0,0,0,0,0
const Ks0108Char_t char_52 = {6, {0x00, 0x70, 0x48, 0x44, 0xfe, 0x40}}; // 4

// original was "53":[0,30,2,30,32,32,34,28,0,0,0,0,0,0,0,0
const Ks0108Char_t char_53 = {6, {0x00, 0x4e, 0x8a, 0x8a, 0x8a, 0x70}}; // 5

// original was "54":[0,28,2,30,34,34,34,28,0,0,0,0,0,0,0,0
const Ks0108Char_t char_54 = {6, {0x00, 0x7c, 0x8a, 0x8a, 0x8a, 0x70}}; // 6

// original was "55":[0,62,34,32,16,8,4,4,0,0,0,0,0,0,0,0
const Ks0108Char_t char_55 = {6, {0x00, 0x06, 0xc2, 0x22, 0x12, 0x0e}}; // 7

// original was "56":[0,28,34,28,34,34,34,28,0,0,0,0,0,0,0,0
const Ks0108Char_t char_56 = {6, {0x00, 0x74, 0x8a, 0x8a, 0x8a, 0x74}}; // 8

// original was "57":[0,28,34,34,34,60,32,28,0,0,0,0,0,0,0,0
const Ks0108Char_t char_57 = {6, {0x00, 0x1c, 0xa2, 0xa2, 0xa2, 0x7c}}; // 9

// original was "58":[0,6,6,0,0,6,6,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_58 = {3, {0x00, 0x66, 0x66}}; // :

// original was "59":[0,6,6,0,0,6,4,2,0,0,0,0,0,0,0,0
const Ks0108Char_t char_59 = {3, {0x00, 0xa6, 0x66}}; // ;

// original was "60":[0,16,12,2,12,16,0,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_60 = {5, {0x00, 0x08, 0x14, 0x14, 0x22}}; // <

// original was "61":[0,0,62,0,62,0,0,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_61 = {6, {0x00, 0x14, 0x14, 0x14, 0x14, 0x14}}; // =

// original was "62":[0,2,12,16,12,2,0,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_62 = {5, {0x00, 0x22, 0x14, 0x14, 0x08}}; // >

// original was "63":[0,28,34,32,16,8,0,8,0,0,0,0,0,0,0,0
const Ks0108Char_t char_63 = {6, {0x00, 0x04, 0x02, 0xa2, 0x12, 0x0c}}; // ?

// original was "64":[120,132,178,170,186,194,28,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_64 = {8, {0x00, 0x3c, 0x42, 0x59, 0x55, 0x1d, 0x21, 0x3e}}; // @

// original was "65":[0,24,36,34,34,62,34,34,0,0,0,0,0,0,0,0
const Ks0108Char_t char_65 = {6, {0x00, 0xf8, 0x24, 0x22, 0x22, 0xfc}}; // A

// original was "66":[0,30,34,34,30,34,34,30,0,0,0,0,0,0,0,0
const Ks0108Char_t char_66 = {6, {0x00, 0xfe, 0x92, 0x92, 0x92, 0x6c}}; // B

// original was "67":[0,60,2,2,2,2,2,60,0,0,0,0,0,0,0,0
const Ks0108Char_t char_67 = {6, {0x00, 0x7c, 0x82, 0x82, 0x82, 0x82}}; // C

// original was "68":[0,30,34,34,34,34,34,30,0,0,0,0,0,0,0,0
const Ks0108Char_t char_68 = {6, {0x00, 0xfe, 0x82, 0x82, 0x82, 0x7c}}; // D

// original was "69":[0,62,2,2,2,30,2,62,0,0,0,0,0,0,0,0
const Ks0108Char_t char_69 = {6, {0x00, 0xfe, 0xa2, 0xa2, 0xa2, 0x82}}; // E

// original was "70":[0,62,2,2,2,30,2,2,0,0,0,0,0,0,0,0
const Ks0108Char_t char_70 = {6, {0x00, 0xfe, 0x22, 0x22, 0x22, 0x02}}; // F

// original was "71":[0,60,2,2,2,50,34,60,0,0,0,0,0,0,0,0
const Ks0108Char_t char_71 = {6, {0x00, 0x7c, 0x82, 0x82, 0xa2, 0xe2}}; // G

// original was "72":[0,34,34,34,34,62,34,34,0,0,0,0,0,0,0,0
const Ks0108Char_t char_72 = {6, {0x00, 0xfe, 0x20, 0x20, 0x20, 0xfe}}; // H

// original was "73":[0,14,4,4,4,4,4,14,0,0,0,0,0,0,0,0
const Ks0108Char_t char_73 = {4, {0x00, 0x82, 0xfe, 0x82}}; // I

// original was "74":[0,14,8,8,8,8,8,6,0,0,0,0,0,0,0,0
const Ks0108Char_t char_74 = {4, {0x00, 0x82, 0x82, 0x7e}}; // J

// original was "75":[0,34,18,10,6,10,18,34,0,0,0,0,0,0,0,0
const Ks0108Char_t char_75 = {6, {0x00, 0xfe, 0x10, 0x28, 0x44, 0x82}}; // K

// original was "76":[0,2,2,2,2,2,2,30,0,0,0,0,0,0,0,0
const Ks0108Char_t char_76 = {5, {0x00, 0xfe, 0x80, 0x80, 0x80}}; // L

// original was "77":[0,102,90,90,66,66,66,66,0,0,0,0,0,0,0,0
const Ks0108Char_t char_77 = {7, {0x00, 0xfe, 0x02, 0x0c, 0x0c, 0x02, 0xfe}}; // M

// original was "78":[0,34,34,34,38,42,50,34,0,0,0,0,0,0,0,0
const Ks0108Char_t char_78 = {6, {0x00, 0xfe, 0x10, 0x20, 0x40, 0xfe}}; // N

// original was "79":[0,28,34,34,34,34,34,28,0,0,0,0,0,0,0,0
const Ks0108Char_t char_79 = {6, {0x00, 0x7c, 0x82, 0x82, 0x82, 0x7c}}; // O

// original was "80":[0,30,34,34,34,34,30,2,0,0,0,0,0,0,0,0
const Ks0108Char_t char_80 = {6, {0x00, 0xfe, 0x42, 0x42, 0x42, 0x3c}}; // P

// original was "81":[0,28,34,34,34,34,42,28,48,0,0,0,0,0,0,0
const Ks0108Char_t char_81 = {6, {0x00, 0x7c, 0x82, 0xc2, 0x82, 0x7c}}; // Q

// original was "82":[0,30,34,34,34,34,30,34,0,0,0,0,0,0,0,0
const Ks0108Char_t char_82 = {6, {0x00, 0xfe, 0x42, 0x42, 0x42, 0xbc}}; // R

// original was "83":[0,60,2,2,28,32,32,30,0,0,0,0,0,0,0,0
const Ks0108Char_t char_83 = {6, {0x00, 0x8c, 0x92, 0x92, 0x92, 0x62}}; // S

// original was "84":[0,62,8,8,8,8,8,8,0,0,0,0,0,0,0,0
const Ks0108Char_t char_84 = {6, {0x00, 0x02, 0x02, 0xfe, 0x02, 0x02}}; // T

// original was "85":[0,34,34,34,34,34,34,28,0,0,0,0,0,0,0,0
const Ks0108Char_t char_85 = {6, {0x00, 0x7e, 0x80, 0x80, 0x80, 0x7e}}; // U

// original was "86":[0,34,34,20,20,20,8,8,0,0,0,0,0,0,0,0
const Ks0108Char_t char_86 = {6, {0x00, 0x06, 0x38, 0xc0, 0x38, 0x06}}; // V

// original was "87":[0,130,130,68,84,84,40,40,0,0,0,0,0,0,0,0
const Ks0108Char_t char_87 = {8, {0x00, 0x06, 0x38, 0xc0, 0x30, 0xc0, 0x38, 0x06}}; // W

// original was "88":[0,34,34,20,28,20,34,34,0,0,0,0,0,0,0,0
const Ks0108Char_t char_88 = {6, {0x00, 0xc6, 0x38, 0x10, 0x38, 0xc6}}; // X

// original was "89":[0,34,34,20,20,8,8,8,0,0,0,0,0,0,0,0
const Ks0108Char_t char_89 = {6, {0x00, 0x06, 0x18, 0xe0, 0x18, 0x06}}; // Y

// original was "90":[0,62,32,16,8,4,2,62,0,0,0,0,0,0,0,0
const Ks0108Char_t char_90 = {6, {0x00, 0xc2, 0xa2, 0x92, 0x8a, 0x86}}; // Z

// original was "91":[0,6,2,2,2,2,2,6,0,0,0,0,0,0,0,0
const Ks0108Char_t char_91 = {3, {0x00, 0xfe, 0x82}}; // [

// original was "92":[0,2,2,4,4,8,8,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_92 = {4, {0x00, 0x06, 0x18, 0x60}}; // \

// original was "93":[0,6,4,4,4,4,4,6,0,0,0,0,0,0,0,0
const Ks0108Char_t char_93 = {3, {0x00, 0x82, 0xfe}}; // ]

// original was "94":[0,8,20,34,0,0,0,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_94 = {6, {0x00, 0x08, 0x04, 0x02, 0x04, 0x08}}; // ^

// original was "95":[0,0,0,0,0,0,0,126,0,0,0,0,0,0,0,0
const Ks0108Char_t char_95 = {7, {0x00, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80}}; // _

// original was "96":[0,2,4,0,0,0,0,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_96 = {3, {0x00, 0x02, 0x04}}; // `

// original was "97":[0,12,16,28,18,18,28,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_97 = {5, {0x00, 0x30, 0x4a, 0x4a, 0x7c}}; // a

// original was "98":[0,2,14,18,18,18,14,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_98 = {5, {0x00, 0x7e, 0x44, 0x44, 0x38}}; // b

// original was "99":[0,12,18,2,2,18,12,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_99 = {5, {0x00, 0x3c, 0x42, 0x42, 0x24}}; // c

// original was "100":[16,16,28,18,18,18,28,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_100 = {5, {0x00, 0x38, 0x44, 0x44, 0x7f}}; // d

// original was "101":[0,12,18,18,30,2,12,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_101 = {5, {0x00, 0x3c, 0x52, 0x52, 0x1c}}; // e

// original was "102":[8,20,4,14,4,4,4,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_102 = {5, {0x00, 0x08, 0x7e, 0x09, 0x02}}; // f

// original was "103":[0,0,28,18,18,28,16,14,0,0,0,0,0,0,0,0
const Ks0108Char_t char_103 = {5, {0x00, 0x98, 0xa4, 0xa4, 0x7c}}; // g

// original was "104":[2,2,14,18,18,18,18,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_104 = {5, {0x00, 0x7f, 0x04, 0x04, 0x78}}; // h

// original was "105":[2,0,2,2,2,2,2,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_105 = {2, {0x00, 0x7d}}; // i

// original was "106":[4,0,4,4,4,4,2,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_106 = {3, {0x00, 0x40, 0x3d}}; // j

// original was "107":[0,0,18,10,6,10,18,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_107 = {5, {0x00, 0x7c, 0x10, 0x28, 0x44}}; // k

// original was "108":[2,2,2,2,2,2,2,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_108 = {2, {0x00, 0x7f}}; // l

// original was "109":[0,0,22,42,42,42,42,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_109 = {6, {0x00, 0x7c, 0x04, 0x78, 0x04, 0x78}}; // m

// original was "110":[0,0,14,18,18,18,18,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_110 = {5, {0x00, 0x7c, 0x04, 0x04, 0x78}}; // n

// original was "111":[0,0,12,18,18,18,12,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_111 = {5, {0x00, 0x38, 0x44, 0x44, 0x38}}; // o

// original was "112":[0,0,14,18,18,14,2,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_112 = {5, {0x00, 0x7c, 0x24, 0x24, 0x18}}; // p

// original was "113":[0,0,28,18,18,28,16,16,0,0,0,0,0,0,0,0
const Ks0108Char_t char_113 = {5, {0x00, 0x18, 0x24, 0x24, 0xfc}}; // q

// original was "114":[0,0,10,6,2,2,2,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_114 = {4, {0x00, 0x7c, 0x08, 0x04}}; // r

// original was "115":[0,0,28,2,12,16,14,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_115 = {5, {0x00, 0x48, 0x54, 0x54, 0x24}}; // s

// original was "116":[4,14,4,4,4,4,24,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_116 = {5, {0x00, 0x02, 0x3f, 0x42, 0x40}}; // t

// original was "117":[0,0,18,18,18,18,28,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_117 = {5, {0x00, 0x3c, 0x40, 0x40, 0x7c}}; // u

// original was "118":[0,0,34,34,20,28,8,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_118 = {6, {0x00, 0x0c, 0x30, 0x60, 0x30, 0x0c}}; // v

// original was "119":[0,0,130,130,84,124,40,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_119 = {8, {0x00, 0x0c, 0x30, 0x60, 0x30, 0x60, 0x30, 0x0c}}; // w

// original was "120":[0,0,34,20,8,20,34,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_120 = {6, {0x00, 0x44, 0x28, 0x10, 0x28, 0x44}}; // x

// original was "121":[0,0,18,18,20,24,16,12,0,0,0,0,0,0,0,0
const Ks0108Char_t char_121 = {5, {0x00, 0x0c, 0x90, 0xa0, 0x7c}}; // y

// original was "122":[0,0,30,16,12,2,30,0,0,0,0,0,0,0,0,0
const Ks0108Char_t char_122 = {5, {0x00, 0x64, 0x54, 0x54, 0x4c}}; // z

const uint16_t charTableSize = 90;

const uint16_t charTable[] = {
    33, 34, 35, 36,     // !, ", #, $,
    37, 38, 39, 40,     // %, &, ', (,
    41, 42, 43, 44,     // ), *, +, ,,
    45, 46, 47, 48,     // -, ., /, 0,
    49, 50, 51, 52,     // 1, 2, 3, 4,
    53, 54, 55, 56,     // 5, 6, 7, 8,
    57, 58, 59, 60,     // 9, :, ;, <,
    61, 62, 63, 64,     // =, >, ?, @,
    65, 66, 67, 68,     // A, B, C, D,
    69, 70, 71, 72,     // E, F, G, H,
    73, 74, 75, 76,     // I, J, K, L,
    77, 78, 79, 80,     // M, N, O, P,
    81, 82, 83, 84,     // Q, R, S, T,
    85, 86, 87, 88,     // U, V, W, X,
    89, 90, 91, 92,     // Y, Z, [, \,
    93, 94, 95, 96,     // ], ^, _, `,
    97, 98, 99, 100,    // a, b, c, d,
    101, 102, 103, 104, // e, f, g, h,
    105, 106, 107, 108, // i, j, k, l,
    109, 110, 111, 112, // m, n, o, p,
    113, 114, 115, 116, // q, r, s, t,
    117, 118, 119, 120, // u, v, w, x,
    121, 122            // y, z
};

const Ks0108Char_t *chars[] = {
    &char_33, &char_34, &char_35, &char_36, &char_37, &char_38, &char_39, &char_40, &char_41, &char_42, &char_43, &char_44,
    &char_45, &char_46, &char_47, &char_48, &char_49, &char_50, &char_51, &char_52, &char_53, &char_54, &char_55, &char_56,
    &char_57, &char_58, &char_59, &char_60, &char_61, &char_62, &char_63, &char_64, &char_65, &char_66, &char_67, &char_68,
    &char_69, &char_70, &char_71, &char_72, &char_73, &char_74, &char_75, &char_76, &char_77, &char_78, &char_79, &char_80,
    &char_81, &char_82, &char_83, &char_84, &char_85, &char_86, &char_87, &char_88, &char_89, &char_90, &char_91, &char_92,
    &char_93, &char_94, &char_95, &char_96, &char_97, &char_98, &char_99, &char_100, &char_101, &char_102, &char_103, &char_104,
    &char_105, &char_106, &char_107, &char_108, &char_109, &char_110, &char_111, &char_112, &char_113, &char_114, &char_115, &char_116,
    &char_117, &char_118, &char_119, &char_120, &char_121, &char_122};

#endif // TMONITOR_KS0108_FONT
       // это окончание автоматически сгенерированного файла шрифта для библиотеки ks0108