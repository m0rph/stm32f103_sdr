#include "dspCICFilter.h"

/**
  * @brief	Initialization function for the Q15 CiC Filter
  * @param[in]	instance: Pointer to Q15 CiC Structure
  * @param[in]	stages: Number of CiC stages
  * @param[in]	tap: CiC filter Tap
  * @param[in]	state: Pointer to the state buffer, of size (tap + 1)
  * @return	None
  */
void DSPCiCFilterInitQ15(DSPCiCFilterInstQ15* instance, uint8_t stages, uint16_t tap, int16_t* state) {
	//fc = fs/tap
	instance->stages = stages;
	instance->tap = tap + 1;
	instance->state = state;
	instance->stateIndex = 0;

	//The gain of the CiC filter is m^stages
	instance->gain = 32768 / tap;

	//Initialize state buffer to 0's
	uint16_t i;
	for(i = 0; i < tap; i++) {
		instance->state[i] = 0;
	}
}

/**
  * @brief	Processing function for the Q15 CiC Filter
  * @param[in]	instance: Pointer to Q15 CiC Structure
  * @param[in]	input: Pointer to the block of input data
  * @param[out]	output: Pointer to the block of output data
  * @param[in]	blockSize: Size/samples of/in the processing block
  * @return	None
  */
void DSPCiCFilterQ15(DSPCiCFilterInstQ15* instance, int16_t* input, int16_t* output, uint16_t blockSize) {
	uint16_t i;
	for(i = 0; i < blockSize; i++) {
		//Integrator part, y(n) = y(n-1) + x(n)
		int16_t yLast = instance->stateIndex - 1;
		if(yLast < 0) {
			yLast = instance->tap - 1;
		}
		instance->state[instance->stateIndex] = instance->state[yLast] + input[i];

		//Comb part, z(n) = y(n) - y(n-M)
		yLast = instance->stateIndex + 1;
		if(yLast >= instance->tap) {
			yLast = 0;
		}
		output[i] = instance->state[instance->stateIndex] - instance->state[yLast];

		//Correct for CiC filter gain
		output[i] = (output[i] * instance->gain) >> 15;

		instance->stateIndex = yLast;
	}
}

/**
  * @brief	Initialization function for the Q15 CiC Decimation Filter
  * @param[in]	instance: Pointer to Q15 CiC Decimate Structure
  * @param[in]	stages: Number of CiC stages
  * @param[in]	m: Decimation factor
  * @param[in]	iState: Pointer to the integrator state buffer, of size m
  * @param[in]	cState: Pointer to the comb state buffer, of size m
  * @return	None
  */
void DSPCiCDecimateInitQ15(DSPCiCDecimateInstQ15* instance, uint8_t stages, uint16_t m, int32_t* iState, int32_t* cState) {
	instance->stages = stages;
	instance->m = m;
	instance->mIndex = 0;
	instance->iState = iState;
	instance->cState = cState;

	//The gain of the CiC decimator is m^stages
	float gain = powf((float)m, (float)stages);
	gain = 1.0f / gain;
	arm_float_to_q15(&gain, &instance->gain, 1);
//	instance->gain = 32768 / (m * stages);

	//Initialize state buffer to 0's
	uint8_t i;
	for(i = 0; i < stages; i++) {
		instance->iState[i] = 0;
		instance->cState[i] = 0;
	}
}

/**
  * @brief	Processing function for the Q15 CiC Decimate
  * @param[in]	instance: Pointer to Q15 CiC Structure
  * @param[in]	input: Pointer to the block of input data
  * @param[out]	output: Pointer to the block of output data
  * @param[in]	blockSize: Size/samples of/in the processing block
  * @return	Number of samples in the output buffer
  */
uint16_t DSPCiCDecimateQ15(DSPCiCDecimateInstQ15* instance, int16_t* input, int16_t* output, uint16_t blockSize) {
	uint16_t outIndex = 0;

	uint16_t i;
	for(i = 0; i < blockSize; i++) {
		//Integrator part, y(n) = y(n-1) + x(n)
		instance->iState[0] = instance->iState[0] + input[i];

		uint8_t s;
		for(s = 1; s < instance->stages; s++) {
			instance->iState[s] = instance->iState[s] + instance->iState[s-1];
		}

		//Increment current decimation status
		instance->mIndex += 1;

		//Decimation part, only keep every M samples
		if(instance->mIndex == instance->m) {
			//Comb part, z(n) = y(n) - y(n-M)
			int32_t comb = instance->iState[instance->stages - 1] - instance->cState[0];
			instance->cState[0] = instance->iState[instance->stages - 1];

			for(s = 1; s < instance->stages; s++) {
				int32_t out = comb - instance->cState[s];
				instance->cState[s] = comb;
				comb = out;
			}

			//Correct for CiC filter gain
			output[outIndex] = (comb * instance->gain) >> 15;

			outIndex += 1;
			instance->mIndex = 0;
		}
	}

	return outIndex;
}

/**
  * @brief	Initialization function for the Q15 CiC Interpolation Filter
  * @param[in]	instance: Pointer to Q15 CiC Interpolate Structure
  * @param[in]	stages: Number of CiC stages
  * @param[in]	l: Interpolation factor
  * @return	None
  */
void DSPCiCInterpolateInitQ15(DSPCiCInterpolateInstQ15* instance, uint8_t stages, uint16_t l) {
	instance->stages = stages;
	instance->l = l;
	instance->lIndex = 0;
	instance->lastInput = 0;
	instance->lastOutput = 0;

	//The gain of the CiC integrator is (l^stages)/l
	instance->gain = 32768 / l;
}

/**
  * @brief	Processing function for the Q15 CiC Interpolate
  * @param[in]	instance: Pointer to Q15 CiC Structure
  * @param[in]	input: Pointer to the block of input data
  * @param[out]	output: Pointer to the block of output data
  * @param[in]	blockSize: Size/samples of/in the processing block
  * @return	Number of samples in the output buffer
  */
uint16_t DSPCiCInterpolateQ15(DSPCiCInterpolateInstQ15* instance, int16_t* input, int16_t* output, uint16_t blockSize) {
	uint16_t inIndex = 0;

	uint16_t i;
	for(i = 0; i < (blockSize * instance->l); i++) {
		int16_t y = 0;
		if(instance->lIndex == instance->l) {
			//Comb part, y(n) = x(n) - x(n-1)
			y = input[inIndex] - instance->lastInput;
			instance->lastInput = input[inIndex];

			inIndex += 1;
			instance->lIndex = 0;
		}

		//Integrator part, z(n) = z(n-1) + y(n)
		output[i] = instance->lastOutput + y;
		instance->lastOutput = output[i];

		//Correct for CiC filter gain
		output[i] = (output[i] * instance->gain) >> 15;

		//Increment current interpolator status
		instance->lIndex += 1;
	}

	return i;
}
