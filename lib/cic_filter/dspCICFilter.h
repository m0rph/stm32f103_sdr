#ifndef DSPCICFILTER_H_
#define DSPCICFILTER_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>

typedef struct {
	uint8_t stages;
	uint16_t tap;
	int16_t* state;
	uint16_t stateIndex;
	int16_t gain;
} DSPCiCFilterInstQ15;

typedef struct {
	uint8_t stages;
	uint16_t m;
	uint16_t mIndex;
	int32_t* iState;
	int32_t* cState;
	int16_t gain;
} DSPCiCDecimateInstQ15;

typedef struct {
	uint8_t stages;
	uint16_t l;
	uint16_t lIndex;
	int16_t lastInput;
	int16_t lastOutput;
	int16_t gain;
} DSPCiCInterpolateInstQ15;

void DSPCiCFilterInitQ15(DSPCiCFilterInstQ15* instance, uint8_t stages, uint16_t tap, int16_t* state);
void DSPCiCFilterQ15(DSPCiCFilterInstQ15* instance, int16_t* input, int16_t* output, uint16_t blockSize);

void DSPCiCDecimateInitQ15(DSPCiCDecimateInstQ15* instance, uint8_t stages, uint16_t m, int32_t* iState, int32_t* cState);
uint16_t DSPCiCDecimateQ15(DSPCiCDecimateInstQ15* instance, int16_t* input, int16_t* output, uint16_t blockSize);

void DSPCiCInterpolateInitQ15(DSPCiCInterpolateInstQ15* instance, uint8_t stages, uint16_t l);
uint16_t DSPCiCInterpolateQ15(DSPCiCInterpolateInstQ15* instance, int16_t* input, int16_t* output, uint16_t blockSize);

#ifdef __cplusplus
}
#endif

#endif /* DSPCICFILTER_H_ */
