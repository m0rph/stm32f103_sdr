#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/i2c.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "si5351.h"
#include <coefficients.h>
#include <pcd8544.h>

#define ARM_MATH_CM3
#include <arm_math.h>
#include <coefficients.h>

#define SI5351_I2C I2C2
//#define SI5351_BUS_BASE_ADDR 0x60
#define OLEDI2C I2C2

#define ADC_SAMPLE_RATE 68000
#define AF_SAMPLE_RATE 68000
#define DC_GAIN_SAMPLE_RATE 68000
#define ADC_SAMPLE_SIZE 1024
#define ADC_SAMPLE_SIZE_HALF ADC_SAMPLE_SIZE / 2
#define DECIMATION_FACTOR 1
#define PWM_BLOCK_SIZE ADC_SAMPLE_SIZE_HALF / DECIMATION_FACTOR

int32_t buffer_adc[ADC_SAMPLE_SIZE];
int16_t buffer_pwm[ADC_SAMPLE_SIZE];
int16_t buffer_adc_right[ADC_SAMPLE_SIZE_HALF];
int16_t buffer_adc_left[ADC_SAMPLE_SIZE_HALF];
int16_t buffer_pwm_right[PWM_BLOCK_SIZE];
int16_t buffer_pwm_left[PWM_BLOCK_SIZE];
int16_t state_right[NO_HILBERT_COEFFS + ADC_SAMPLE_SIZE_HALF]; // coeff_count + instanace_sample_count
int16_t state_left[NO_HILBERT_COEFFS + ADC_SAMPLE_SIZE_HALF];  // coeff_count + instanace_sample_count
arm_fir_instance_q15 fir_right, fir_left;
volatile uint16_t adc_buffer_pointer = 0;
uint32_t frequency = 3699000;
bool adc_interrupt = false;

void delay_ms(uint32_t n)
{
    for (size_t i = 0; i < n; i++)
        __asm__("nop");
}

void rcc_clock_setup_in_hse_8mhz_out_128mhz()
{
    rcc_osc_on(RCC_HSI);
    rcc_wait_for_osc_ready(RCC_HSI);
    rcc_set_sysclk_source(RCC_CFGR_SW_SYSCLKSEL_HSICLK);
    rcc_osc_on(RCC_HSE);
    rcc_wait_for_osc_ready(RCC_HSE);
    rcc_set_sysclk_source(RCC_CFGR_SW_SYSCLKSEL_HSECLK);
    rcc_set_hpre(RCC_CFGR_HPRE_SYSCLK_NODIV);   /* set it to 128MHz */
    rcc_set_adcpre(RCC_CFGR_ADCPRE_PCLK2_DIV8); /* 128/8 = 16Mhz, Max. 14MHz */
    rcc_set_ppre1(RCC_CFGR_PPRE1_HCLK_DIV2);    // 128/2 = 64MHz, Max: 36MHz
    rcc_set_ppre2(RCC_CFGR_PPRE2_HCLK_NODIV);   /* Set. 128MHz Max. 72MHz */
    flash_set_ws(FLASH_ACR_LATENCY_2WS);
    rcc_set_pll_multiplication_factor(RCC_CFGR_PLLMUL_PLL_CLK_MUL16);
    rcc_set_pll_source(RCC_CFGR_PLLSRC_HSE_CLK);
    rcc_set_pllxtpre(RCC_CFGR_PLLXTPRE_HSE_CLK);
    rcc_osc_on(RCC_PLL);
    rcc_wait_for_osc_ready(RCC_PLL);
    rcc_set_sysclk_source(RCC_CFGR_SW_SYSCLKSEL_PLLCLK);
    rcc_ahb_frequency = 128000000;
    rcc_apb1_frequency = 64000000;
    rcc_apb2_frequency = 128000000;
}

static void clock_setup(void)
{
    rcc_clock_setup_in_hse_8mhz_out_128mhz(); // or 128mhz
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_GPIOC);
    rcc_periph_clock_enable(RCC_DMA1);
    rcc_periph_clock_enable(RCC_ADC1);
    rcc_periph_clock_enable(RCC_ADC2);
    rcc_periph_clock_enable(RCC_I2C2);
    rcc_periph_clock_enable(RCC_AFIO);
    rcc_periph_clock_enable(RCC_TIM1);
    rcc_periph_clock_enable(RCC_TIM2);
    rcc_periph_clock_enable(RCC_TIM3);
    rcc_periph_clock_enable(RCC_TIM4);
    rcc_periph_clock_enable(RCC_SPI2);
}

void adc_iq_setup(void)
{
    gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_ANALOG, GPIO1);
    gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_ANALOG, GPIO2);
    adc_power_off(ADC1);
    adc_power_off(ADC2);
    adc_set_dual_mode(ADC_CR1_DUALMOD_RSM);
    adc_set_sample_time(ADC1, 1, ADC_SMPR_SMP_239DOT5CYC);
    adc_set_sample_time(ADC2, 2, ADC_SMPR_SMP_239DOT5CYC);
    adc_enable_external_trigger_regular(ADC1, ADC_CR2_EXTSEL_TIM3_TRGO); // ADC_CR2_EXTSEL_SWSTART
    adc_enable_external_trigger_regular(ADC2, ADC_CR2_EXTSEL_TIM3_TRGO); // ADC_CR2_EXTSEL_TIM3_TRGO
    adc_power_on(ADC1);
    adc_power_on(ADC2);
    delay_ms(10);
    adc_reset_calibration(ADC1);
    adc_reset_calibration(ADC2);
    adc_calibrate(ADC1);
    adc_calibrate(ADC2);
    uint8_t ADC1_channel_seq[1] = {1};
    uint8_t ADC2_channel_seq[1] = {2};
    adc_set_regular_sequence(ADC1, 1, ADC1_channel_seq);
    adc_set_regular_sequence(ADC2, 1, ADC2_channel_seq);
    adc_enable_dma(ADC1);
    delay_ms(100);
}

// void adc_iq_setup(void)
// {
//     // rcc_set_adcpre(RCC_CFGR_ADCPRE_PCLK2_DIV6);
//     gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_ANALOG, GPIO1);
//     gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_ANALOG, GPIO2);
//     adc_power_off(ADC1);
//     adc_power_off(ADC2);
//     adc_set_dual_mode(ADC_CR1_DUALMOD_RSM);
//     adc_set_sample_time(ADC1, 1, ADC_SMPR_SMP_239DOT5CYC);
//     adc_set_sample_time(ADC2, 2, ADC_SMPR_SMP_239DOT5CYC);
//     adc_enable_external_trigger_regular(ADC1, ADC_CR2_SWSTART); // ADC_CR2_EXTSEL_SWSTART
//     adc_enable_external_trigger_regular(ADC2, ADC_CR2_SWSTART); // ADC_CR2_EXTSEL_TIM3_TRGO
//     adc_power_on(ADC1);
//     adc_power_on(ADC2);
//     delay(10);
//     adc_reset_calibration(ADC1);
//     adc_reset_calibration(ADC2);
//     adc_calibrate(ADC1);
//     adc_calibrate(ADC2);
//     uint8_t ADC1_channel_seq[1] = {1};
//     uint8_t ADC2_channel_seq[1] = {2};
//     adc_set_regular_sequence(ADC1, 1, ADC1_channel_seq);
//     adc_set_regular_sequence(ADC2, 1, ADC2_channel_seq);
//     adc_enable_dma(ADC1);
//     delay(100);
// }

void dma_adc_setup(void)
{
    dma_disable_channel(DMA1, DMA_CHANNEL1);
    dma_enable_circular_mode(DMA1, DMA_CHANNEL1);
    dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL1);
    dma_set_peripheral_size(DMA1, DMA_CHANNEL1, DMA_CCR_PSIZE_32BIT);
    dma_set_memory_size(DMA1, DMA_CHANNEL1, DMA_CCR_MSIZE_32BIT);
    dma_set_read_from_peripheral(DMA1, DMA_CHANNEL1);
    dma_set_peripheral_address(DMA1, DMA_CHANNEL1, (uint32_t)&ADC1_DR);
    dma_set_memory_address(DMA1, DMA_CHANNEL1, (uint32_t)&buffer_adc);
    dma_set_number_of_data(DMA1, DMA_CHANNEL1, ADC_SAMPLE_SIZE);
    dma_enable_half_transfer_interrupt(DMA1, DMA_CHANNEL1);
    dma_enable_transfer_complete_interrupt(DMA1, DMA_CHANNEL1);
    nvic_enable_irq(NVIC_DMA1_CHANNEL1_IRQ);
    dma_enable_channel(DMA1, DMA_CHANNEL1);
}

void dma_pwm_setup()
{
    dma_disable_channel(DMA1, DMA_CHANNEL3);
    dma_enable_circular_mode(DMA1, DMA_CHANNEL3);
    dma_disable_peripheral_increment_mode(DMA1, DMA_CHANNEL3);
    dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL3);
    dma_set_peripheral_size(DMA1, DMA_CHANNEL3, DMA_CCR_PSIZE_16BIT);
    dma_set_memory_size(DMA1, DMA_CHANNEL3, DMA_CCR_MSIZE_16BIT);
    dma_set_read_from_memory(DMA1, DMA_CHANNEL3);
    dma_set_peripheral_address(DMA1, DMA_CHANNEL3, (uint32_t)&TIM2_CCR1);
    dma_set_memory_address(DMA1, DMA_CHANNEL3, (uint32_t)&buffer_pwm);
    dma_set_number_of_data(DMA1, DMA_CHANNEL3, ADC_SAMPLE_SIZE);
    dma_enable_channel(DMA1, DMA_CHANNEL3);
}

void tim1_setup()
{
    gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO11);
    timer_set_oc_mode(TIM1, TIM_OC4, TIM_OCM_PWM1);
    timer_enable_oc_output(TIM1, TIM_OC4);
    timer_enable_break_main_output(TIM1);
    timer_set_oc_value(TIM1, TIM_OC4, (rcc_ahb_frequency / DC_GAIN_SAMPLE_RATE) * 0.2);
    timer_disable_oc_clear(TIM1, TIM_OC4);
    timer_set_prescaler(TIM1, 0);
    timer_set_period(TIM1, rcc_ahb_frequency / DC_GAIN_SAMPLE_RATE);
    timer_enable_counter(TIM1);
}

void tim2_setup()
{
    gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO0);
    timer_set_prescaler(TIM2, 1);
    timer_set_period(TIM2, rcc_ahb_frequency / AF_SAMPLE_RATE);
    timer_set_oc_mode(TIM2, TIM_OC1, TIM_OCM_PWM1);
    timer_enable_oc_output(TIM2, TIM_OC1);
    timer_enable_counter(TIM2);
}

static void tim3_setup(void)
{
    timer_set_prescaler(TIM3, 1);
    timer_set_period(TIM3, rcc_ahb_frequency / ADC_SAMPLE_RATE);
    timer_set_master_mode(TIM3, TIM_CR2_MMS_UPDATE);
    timer_enable_irq(TIM3, TIM_DIER_UDE);
    timer_enable_counter(TIM3);
}

static void tim4_setup(void)
{
    timer_set_period(TIM4, 88);
    timer_slave_set_mode(TIM4, TIM_SMCR_SMS_EM3);
    timer_ic_set_input(TIM4, TIM_IC1, TIM_IC_IN_TI1);
    timer_ic_set_input(TIM4, TIM_IC2, TIM_IC_IN_TI2);
    timer_enable_counter(TIM4);
}

static void i2c_setup(void)
{
    gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN, GPIO_I2C2_SCL | GPIO_I2C2_SDA);
    i2c_peripheral_disable(I2C2);
    i2c_set_speed(I2C2, i2c_speed_fm_400k, 64);
    i2c_peripheral_enable(I2C2);
}

void si5351_set_freq_90(uint64_t freq)
{
    uint8_t coef = 650000000 / freq; // 650000000
    uint64_t pll_freq = coef * freq;
    set_freq_manual(freq * 100, pll_freq * 100, SI5351_CLK0);
    set_freq_manual(freq * 100, pll_freq * 100, SI5351_CLK1);
    set_phase(SI5351_CLK0, 0);
    set_phase(SI5351_CLK1, coef);
    pll_reset(SI5351_PLLA);
}

void dma1_channel1_isr()
{
    if (dma_get_interrupt_flag(DMA1, DMA_CHANNEL1, DMA_TCIF))
    {
        adc_buffer_pointer = ADC_SAMPLE_SIZE_HALF;
    }
    if (dma_get_interrupt_flag(DMA1, DMA_CHANNEL1, DMA_HTIF))
    {
        adc_buffer_pointer = 0;
    }
    for (size_t i = 0; i < ADC_SAMPLE_SIZE_HALF; i++)
    {
        buffer_adc_right[i] = buffer_adc[i + adc_buffer_pointer]; //* gain
        buffer_adc_left[i] = (buffer_adc[i + adc_buffer_pointer] >> 16);
    }
    arm_fir_fast_q15(&fir_right, &buffer_adc_right, &buffer_pwm_right, ADC_SAMPLE_SIZE_HALF);
    arm_fir_fast_q15(&fir_left, &buffer_adc_left, &buffer_pwm_left, ADC_SAMPLE_SIZE_HALF);
    arm_add_q15(&buffer_pwm_right, &buffer_pwm_left, &buffer_pwm[adc_buffer_pointer], PWM_BLOCK_SIZE);
    dma_clear_interrupt_flags(DMA1, DMA_CHANNEL1, DMA_TCIF | DMA_HTIF);
}

uint16_t map(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

static void pcd8544_setup(void)
{
    /* Configure GPIOs: SS=PCD8544_SPI_SS, SCK=PCD8544_SPI_SCK, MISO=UNUSED and MOSI=PCD8544_SPI_MOSI */
    gpio_set_mode(PCD8544_SPI_PORT, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
                  PCD8544_SPI_MOSI | PCD8544_SPI_SCK | PCD8544_SPI_SS);
    /* Reset SPI, SPI_CR1 register cleared, SPI is disabled */
    spi_reset(PCD8544_SPI);
    /* Set up SPI in Master mode with:
     * Clock baud rate: 1/64 of peripheral clock frequency
     * Clock: CPOL CPHA (0:0)
     * Data frame format: 8-bit
     * Frame format: MSB First
     */
    spi_init_master(PCD8544_SPI, SPI_CR1_BAUDRATE_FPCLK_DIV_8, SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE,
                    SPI_CR1_CPHA_CLK_TRANSITION_1, SPI_CR1_DFF_8BIT, SPI_CR1_MSBFIRST);

    /*
     * Set NSS management to software.
     *
     * Note:
     * Setting nss high is very important, even if we are controlling the GPIO
     * ourselves this bit needs to be at least set to 1, otherwise the spi
     * peripheral will not send any data out.
     */
    spi_enable_software_slave_management(PCD8544_SPI);
    spi_set_nss_high(PCD8544_SPI);

    /* Enable SPI1 periph. */
    spi_enable(PCD8544_SPI);

    /* Configure GPIOs: DC, SCE, RST */
    gpio_set_mode(PCD8544_RST_PORT, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL,
                  PCD8544_RST);
    gpio_set_mode(PCD8544_DC_PORT, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL,
                  PCD8544_DC);
    gpio_set_mode(PCD8544_SCE_PORT, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL,
                  PCD8544_SCE);

    gpio_set(PCD8544_RST_PORT, PCD8544_RST);
    gpio_set(PCD8544_SCE_PORT, PCD8544_SCE);

    for (int i = 0; i < 200; i++)
        __asm__("nop");
}

int main(void)
{
    clock_setup();
    // i2c_setup();
    //  si5351_init(SI5351_CRYSTAL_LOAD_10PF, 27004500, 0);
    //  si5351_drive_strength(SI5351_CLK0, SI5351_DRIVE_4MA);
    //  si5351_drive_strength(SI5351_CLK1, SI5351_DRIVE_4MA);
    //  si5351_set_freq_90(frequency);
    uint16_t min_gain = rcc_ahb_frequency / DC_GAIN_SAMPLE_RATE * 0.1;
    uint16_t max_gain = rcc_ahb_frequency / DC_GAIN_SAMPLE_RATE * 0.30;
    adc_iq_setup();
    dma_pwm_setup();
    dma_adc_setup();
    tim1_setup();
    tim2_setup();
    tim3_setup();
    tim4_setup();
    arm_fir_init_q15(&fir_right, NO_HILBERT_COEFFS, hilbert_plus_2200_68000, state_right, ADC_SAMPLE_SIZE_HALF);
    arm_fir_init_q15(&fir_left, NO_HILBERT_COEFFS, hilbert_minus_2200_68000, state_left, ADC_SAMPLE_SIZE_HALF);
    pcd8544_setup();
    pcd8544_init();
    pcd8544_clearDisplay();

    wchar_t buffer[30];

    //   gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
    uint16_t enc_pos = 10,
             old_pos;
    while (1)
    {
        enc_pos = timer_get_counter(TIM4) >> 2;
        if (enc_pos != old_pos && enc_pos <= 20 && enc_pos >= 0)
        {
            old_pos = enc_pos;
            timer_set_oc_value(TIM1, TIM_OC4, map(enc_pos, 0, 20, min_gain, max_gain));
            swprintf(buffer, 30, L"vol: %d", enc_pos);
            pcd8544_clearDisplay();
            pcd8544_drawRect(0, 0, map(enc_pos, 0, 20, 0, 83), 5, BLACK);
            pcd8544_drawText(10, 10, BLACK, buffer);
            pcd8544_display();
        }
        else if (enc_pos != old_pos)
        {
            timer_set_counter(TIM4, old_pos << 2);
        }
    }
    return 0;
}
